$(document).ready(function(){
    //slider.
    //if the index.html is loaded proced with the slider.
    if(window.location.href.indexOf('index') > -1){
        $('.galeria').bxSlider({
            mode: 'fade',
            captions: true,
            slideWidth: 1200,
            responsive: true,
            pager: true
        })//end of bxSlider().
    }
    //posts
    if(window.location.href.indexOf('index') > -1){
        let posts = [
            {
                title: 'Prueba de titulo 1',
                date: 'Publicado el dia ' + moment().date() + ' de ' + moment().format('MMMM') + ' del ' + moment().format('YYYY') ,
                content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac sagittis nisi, et sollicitudin erat. Pellentesque auctor elementum fringilla. Sed cursus elit lectus, ut pharetra sapien pharetra luctus. Cras blandit risus magna, eu posuere sem lobortis a. Mauris et vehicula odio. Aenean porttitor tempus erat, ut porttitor elit. Donec eget ullamcorper dolor. Nunc auctor nulla vitae lorem finibus pharetra. Duis non ligula erat. Quisque nec ante vitae dui ultricies semper.'
            },
            {
                title: 'Prueba de titulo 2',
                date: 'Publicado el dia ' + moment().date() + ' de ' + moment().format('MMMM') + ' del ' + moment().format('YYYY') ,
                content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac sagittis nisi, et sollicitudin erat. Pellentesque auctor elementum fringilla. Sed cursus elit lectus, ut pharetra sapien pharetra luctus. Cras blandit risus magna, eu posuere sem lobortis a. Mauris et vehicula odio. Aenean porttitor tempus erat, ut porttitor elit. Donec eget ullamcorper dolor. Nunc auctor nulla vitae lorem finibus pharetra. Duis non ligula erat. Quisque nec ante vitae dui ultricies semper.'
            },
            {
                title: 'Prueba de titulo 3',
                date: 'Publicado el dia ' + moment().date() + ' de ' + moment().format('MMMM') + ' del ' + moment().format('YYYY') ,
                content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac sagittis nisi, et sollicitudin erat. Pellentesque auctor elementum fringilla. Sed cursus elit lectus, ut pharetra sapien pharetra luctus. Cras blandit risus magna, eu posuere sem lobortis a. Mauris et vehicula odio. Aenean porttitor tempus erat, ut porttitor elit. Donec eget ullamcorper dolor. Nunc auctor nulla vitae lorem finibus pharetra. Duis non ligula erat. Quisque nec ante vitae dui ultricies semper.'
            },
            {
                title: 'Prueba de titulo 4',
                date: 'Publicado el dia ' + moment().date() + ' de ' + moment().format('MMMM') + ' del ' + moment().format('YYYY') ,
                content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac sagittis nisi, et sollicitudin erat. Pellentesque auctor elementum fringilla. Sed cursus elit lectus, ut pharetra sapien pharetra luctus. Cras blandit risus magna, eu posuere sem lobortis a. Mauris et vehicula odio. Aenean porttitor tempus erat, ut porttitor elit. Donec eget ullamcorper dolor. Nunc auctor nulla vitae lorem finibus pharetra. Duis non ligula erat. Quisque nec ante vitae dui ultricies semper.'
            },
            {
                title: 'Prueba de titulo 5',
                date: 'Publicado el dia ' + moment().date() + ' de ' + moment().format('MMMM') + ' del ' + moment().format('YYYY') ,
                content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac sagittis nisi, et sollicitudin erat. Pellentesque auctor elementum fringilla. Sed cursus elit lectus, ut pharetra sapien pharetra luctus. Cras blandit risus magna, eu posuere sem lobortis a. Mauris et vehicula odio. Aenean porttitor tempus erat, ut porttitor elit. Donec eget ullamcorper dolor. Nunc auctor nulla vitae lorem finibus pharetra. Duis non ligula erat. Quisque nec ante vitae dui ultricies semper.'
            },
            {
                title: 'Prueba de titulo 6',
                date: 'Publicado el dia ' + moment().date() + ' de ' + moment().format('MMMM') + ' del ' + moment().format('YYYY') ,
                content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac sagittis nisi, et sollicitudin erat. Pellentesque auctor elementum fringilla. Sed cursus elit lectus, ut pharetra sapien pharetra luctus. Cras blandit risus magna, eu posuere sem lobortis a. Mauris et vehicula odio. Aenean porttitor tempus erat, ut porttitor elit. Donec eget ullamcorper dolor. Nunc auctor nulla vitae lorem finibus pharetra. Duis non ligula erat. Quisque nec ante vitae dui ultricies semper.'
            }
        ]
        
        posts.forEach((item, index) => {
            let post = `
                <article class="post">
                    <h2>${item.title}</h2>
                    <span class="date">${item.date}</span>
                    <p>
                        ${item.content}
                    </p>
                    <a href="#" class="button-more">Leer mas</a>
                </article>
            `
            $('#posts').append(post)
        })//end of forEach
    }
    //theme selection
    let theme = $('#theme')
    // if(typeof(Storage) !== 'undefined')   {
    //     console.log('available')
    // }else{
    //     console.log('no available') 
    // }

    $('#to-green').click(function(){
        theme.attr('href', 'css/green.css')
        localStorage.setItem('theme_id', '#to-green')
        localStorage.setItem('currentColor', 'css/green.css')
    })
    $('#to-red').click(function(){
        theme.attr('href', 'css/red.css')
        localStorage.setItem('theme_id', '#to-red')
        localStorage.setItem('currentColor', 'css/red.css')
    })
    $('#to-blue').click(function(){
        theme.attr('href', 'css/blue.css')
        localStorage.setItem('theme_id', '#to-blue')
        localStorage.setItem('currentColor', 'css/blue.css')
    })
    
    //load page with the saved color (currentColor) if this is
    //not the first time.    
    let currentColor = localStorage.getItem('currentColor')
    if(currentColor != null) {    
        $('#theme').attr('href', currentColor)
    }
    
    //scroll on top of the page.
    $('.arriba').click(function(e){
        e.preventDefault() //ignores the default link positioning.
        $('html, body').animate({
            scrollTop: 0 //position to the 0 pixel.
        }, 500)//end of animate() of half of second.
        return false
    }) //end of click().

    //dummy login.
    //store the name gotten from the form.
    $('#login form').submit(function() {
        let form_name = $('#form_name').val()
        localStorage.setItem('form_name', form_name)
    })

    //retrieve the name from the local strorage.
    let form_name = localStorage.getItem('form_name')
    if(form_name != null &&
       form_name != 'undefined'){
        let about_parrafo = $('#about p')        
        about_parrafo.html('<br><strong>Bienvenido, ' + form_name + '</strong>')
        about_parrafo.append("<a href='#' id='logout'> Cerrar sesion</a>")

        $('#login').hide()

        $('#logout').click(function(){
            localStorage.clear()//remove all local storage vars.
            location.reload()//reload the full page.
        })//end of click().
    }
    
    //accordion handling.
    if(window.location.href.indexOf('about') > -1){
        $('#acordeon').accordion()
    }

    //clock handling.
    if(window.location.href.indexOf('reloj') > -1){
        setInterval(function(){
            let reloj = moment().format('hh:mm:ss')        
            $('#reloj').html(reloj)
        }, 1000) //end of setInterval()     
    }

    //validation
    if(window.location.href.indexOf('contact') > -1){
        //add a datepicker.
        $("form input[name='date']").datepicker({
            dateFormat: 'dd-mm-yy'
        })

        //work with the validation plugin.
        $.validate({
            lang: 'en',
            errorMessagePosition: 'top',
            scrollToTopOnError: true
        })
    }
})//end of ready()
