'use strict';
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
//decorator. implemented in lesson 161.
function estampar(logo) {
    return function (target) {
        target.prototype.estampacion = function () {
            console.log("camiseta estampada con el logo de: " + logo);
        };
    };
}
//clase.
var Camiseta = /** @class */ (function () {
    //metodos.
    //this constructor was implemented in lesson 157.
    function Camiseta(color, modelo, marca, talla, precio) {
        this.color = color;
        this.modelo = modelo;
        this.marca = marca;
        this.talla = talla;
        this.precio = precio;
    }
    Camiseta.prototype.setColor = function (color) {
        this.color = color;
    };
    Camiseta.prototype.getColor = function () {
        return this.color;
    };
    Camiseta.prototype.estampacion = function () {
    };
    Camiseta = __decorate([
        estampar('Gucci gang')
    ], Camiseta);
    return Camiseta;
}());
//this code was commented in lesson 158 imports & exports.
/* //var camiseta = new Camiseta()
var camiseta = new Camiseta("rojo", "manga larga", "nike", "L", 14)
//camiseta.color = "rojo"
camiseta.setColor("Rojo")
/* camiseta.modelo = "manga larga"
camiseta.marca = "nike"
camiseta.talla = "L"
camiseta.precio = 10 */
//var playera = new Camiseta()
//playera.color = "azul"
//playera.setColor("azul")
/* playera.modelo = "manga corta"
playera.marca = "adidas"
playera.talla = "L"
playera.precio = 15 */
//console.log(camiseta, playera)
//console.log(camiseta) */
//end of commented code because lesson 158 imports & exports.
//inheritance.
var Sudadera = /** @class */ (function (_super) {
    __extends(Sudadera, _super);
    function Sudadera() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Sudadera.prototype.setCapucha = function (capucha) {
        this.capucha = capucha;
    };
    Sudadera.prototype.getCapucha = function () {
        return this.capucha;
    };
    return Sudadera;
}(Camiseta));
var camiseta = new Camiseta("aaa", "bbb", "ccc", "ddd", 15);
console.log(camiseta);
camiseta.estampacion();
var sudadera_nike = new Sudadera("rojo", "manga larga", "nike", "L", 20);
sudadera_nike.setCapucha(true);
sudadera_nike.setColor("gris jaspeado");
console.log(sudadera_nike);
