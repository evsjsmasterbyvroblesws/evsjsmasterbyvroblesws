//string.
var cadena = "victorroblesweb.es";
//number.
var numero = 12;
//boolean.
var verdadero_falso = true;
//any. it allows any type.
var cualquiera = true;
//arrays.
var lenguajes = ["PHP", "JS", "CSS"];
var years = [12, 13, 14];
//multiple data type.
var cadena2 = "victorroblesweb.es";
cadena2 = 12;
var cadena3 = "kdjkdjdjd";
//let vas var
var numero1 = 10;
var numero2 = 12;
if (numero1 == 10) {
    var numero1_1 = 44;
    var numero2 = 55;
    console.log(numero1_1, numero2);
}
console.log(cadena, numero, verdadero_falso, cualquiera, lenguajes, years, cadena2, cadena3, numero1, numero2);
