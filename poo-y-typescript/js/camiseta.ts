'use strict'
//interface.
interface CamisetaBase{
    setColor(color);
    getColor();
}

//decorator. implemented in lesson 161.
function estampar(logo: string) {
    return function(target: Function){
        target.prototype.estampacion = function():void{
            console.log("camiseta estampada con el logo de: " + logo)
        }
    }
}


//clase.
@estampar('Gucci gang')
class Camiseta implements CamisetaBase { //lesson 159
//class Camiseta { lesson 156
//export class Camiseta { //lesson 158.
//propiedades.
    //public color: string
    private color: string
    private modelo: string
    private marca: string
    private talla: string
    private precio: number

//metodos.
    //this constructor was implemented in lesson 157.
    constructor(color, modelo, marca, talla, precio){
        this.color = color
        this.modelo = modelo
        this.marca = marca
        this.talla = talla
        this.precio = precio
    }
    public setColor(color){
        this.color = color
    }
    public getColor(){
        return this.color
    }

    public estampacion() {

    }
}

//this code was commented in lesson 158 imports & exports.
/* //var camiseta = new Camiseta()
var camiseta = new Camiseta("rojo", "manga larga", "nike", "L", 14)
//camiseta.color = "rojo"
camiseta.setColor("Rojo")
/* camiseta.modelo = "manga larga"
camiseta.marca = "nike"
camiseta.talla = "L"
camiseta.precio = 10 */

//var playera = new Camiseta()
//playera.color = "azul"
//playera.setColor("azul")
/* playera.modelo = "manga corta"
playera.marca = "adidas"
playera.talla = "L"
playera.precio = 15 */
//console.log(camiseta, playera)
//console.log(camiseta) */
//end of commented code because lesson 158 imports & exports.


//inheritance.
class Sudadera extends Camiseta{
    public capucha: boolean;
    setCapucha(capucha: boolean){
        this .capucha = capucha
    }

    getCapucha():boolean{
        return this.capucha
    }
}

var camiseta = new Camiseta("aaa", "bbb", "ccc", "ddd", 15)
console.log(camiseta)
camiseta.estampacion()

var sudadera_nike = new Sudadera("rojo", "manga larga", "nike", "L", 20)
sudadera_nike.setCapucha(true)
sudadera_nike.setColor("gris jaspeado")
console.log(sudadera_nike)