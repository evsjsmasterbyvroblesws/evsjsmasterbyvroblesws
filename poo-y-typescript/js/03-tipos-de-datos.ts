//string.
let cadena: string = "victorroblesweb.es"
//number.
let numero: number = 12
//boolean.
let verdadero_falso: boolean = true
//any. it allows any type.
let cualquiera: any = true
//arrays.
var lenguajes: Array<string> = ["PHP", "JS", "CSS"]
let years: number[] = [12, 13, 14]

//multiple data type.
let cadena2: string|number = "victorroblesweb.es"
cadena2 = 12

//custom types.
type alfanumerico = string | number
let cadena3: alfanumerico = "kdjkdjdjd"

//let vas var
var numero1 = 10
var numero2 = 12
if(numero1 == 10){
    let numero1 = 44;
    var numero2 = 55
    console.log(numero1, numero2)
}

console.log(cadena, numero, verdadero_falso, cualquiera, lenguajes,
            years, cadena2, cadena3, numero1, numero2)