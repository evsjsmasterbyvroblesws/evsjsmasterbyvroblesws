import { Component, OnInit } from '@angular/core'
import { Zapatilla } from '../models/zapatilla'
import { ZapatillaService } from '../services/zapatilla.service'

@Component({ 
    selector: 'zapatillas',
    templateUrl: './zapatillas.component.html',
    providers: [ZapatillaService]

})

export class ZapatillasComponent implements OnInit{
    public titulo: string = "Mi Componente de zapatillas"
    public zapatillas!: Array<Zapatilla> //before services implementation.
    public marcas: string[]
    public color: string
    public mi_marca: string

    constructor(
        private _zapatillaService: ZapatillaService
    ){
        this.mi_marca = 'Fila'
        this.color = 'blue'
        /* this.zapatillas = [ before services implementation.
            new Zapatilla('nike airmax', 'nike', 'rojo', 190, true),
            new Zapatilla('reebok classic', 'reebok', 'blanco', 80, true),
            new Zapatilla('reebok spartan', 'reebok', 'negro', 180, true),
            new Zapatilla('nike runner md', 'nike', 'negro', 60, true),
            new Zapatilla('adidas yezzy', 'adidas', 'gris', 180, false)
        ] */
        this.marcas = new Array()
    }

    ngOnInit(){
        //console.log(this.zapatillas) before services implementation.
        this.zapatillas = this._zapatillaService.getZapatillas()
        alert(this._zapatillaService.getTexto())
        this.getMarcas()
    }

    getMarcas(){
        this.zapatillas.forEach((zapatilla, index) =>{
            if(this.marcas.indexOf(zapatilla.marca) < 0){
                this.marcas.push(zapatilla.marca)
            }            
        })

        console.log(this.marcas)
    }

    getMarca(){
        alert(this.mi_marca)
    }

    addMarca(){
        this.marcas.push(this.mi_marca)
    }

    borrarMarca(indice: number){
        //delete this.marcas[indice] alternative 1.
        this.marcas.splice(indice, 1) //alternative 2 (much better).
    }

    onBlur(){
        console.log("has salido del input.")
    }

    mostrarPalabra(){
       alert(this.mi_marca) 
    }
}
