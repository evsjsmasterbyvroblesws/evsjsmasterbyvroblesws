//import angular router modules.
import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

//import components.
import { ZapatillasComponent } from "./zapatillas/zapatillas.component";
import { VideojuegoComponent } from "./videojuego/videojuego.component";
import { CursosComponent } from "./cursos/cursos.component";
import { HomeComponent } from "./home/home.component";
import { ExternoComponent } from "./externo/externo.component";

//routes array.
const appRoutes: Routes = [
    {path: '', component: HomeComponent},
    {path: 'zapatillas', component: ZapatillasComponent},
    {path: 'videojuego', component: VideojuegoComponent},
    {path: 'cursos', component: CursosComponent},
    //{path: 'cursos/:nombre', component: CursosComponent}, //nombre param is optional, initial code.
    //{path: 'cursos/:nombre/:apellidos', component: CursosComponent}, apellidos param is optional.
    {path: 'cursos/:nombre/:followers', component: CursosComponent}, //followers parm is mandatory.
    {path: 'externo', component: ExternoComponent},
    {path: '**', component: HomeComponent}
]

//export the router module.
export const appRoutingProviders: any[] = [] //router sercice.
export const routing: ModuleWithProviders<any> = RouterModule.forRoot(appRoutes)