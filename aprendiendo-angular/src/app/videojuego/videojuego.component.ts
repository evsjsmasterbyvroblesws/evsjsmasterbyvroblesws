import { Component, OnInit, DoCheck, OnDestroy } from '@angular/core';

@Component({ //decorator.
    selector: 'videojuego',
    //template defines an internal template.
    // template: `
    //     <h2>Componente de Videojuego</h2>
    //     <ul>
    //         <li>GTA</li>
    //         <li>Prince of Persia</li>
    //         <li>Tekken</li>
    //         <li>Mario</li>
    //     </ul>
    // `
    //templateUrl defines an external template.
    templateUrl: './videojuego.component.html'

})
export class VideojuegoComponent implements OnInit, DoCheck, OnDestroy { //class definition. 
    //attributes:
    public titulo: string
    public listado: string   
    constructor(){
        //for testing purpose
        //console.log('se ha cargado el componente: videojuego.component.ts');
        this.titulo = 'Mi Componente de videojuegos'
        this.listado = 'Listado de los juegos mas populares:'
        //console.log("constructor() has been executed!")
    }
    ngOnInit(): void {
        //console.log("ngOnOnit() has been executed!")
    }

    ngDoCheck(): void {
        //console.log("ngDoCheck() has been executed!")
    }
    
    ngOnDestroy(){
        //console.log("ngOnDestroy() has been executed!")
    }
    
    cambiarTitulo(){
        this.titulo = "Nuevo titulo"
    }
}