import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'cursos',
  templateUrl: './cursos.component.html',
  styleUrls: ['./cursos.component.css']
})
export class CursosComponent implements OnInit {
  public nombre: string = ''
  public followers: number = 0

  constructor(
    private _route: ActivatedRoute,
    private _router: Router    
  ) { }

  ngOnInit(): void {
    this._route.params.subscribe((params: Params) => {
      this.nombre = params.nombre
      //this.nombre = params['nombre'] alternative
      this.followers = +params.followers
      //console.log(params)

      if(this.nombre == 'ninguno'){//for test use localhost:4200/cursos/ninguno/12
        this._router.navigate(['/home'])
      }
    })
  }

  redirigir(){
    this._router.navigate(['/zapatillas'])
  }

}
