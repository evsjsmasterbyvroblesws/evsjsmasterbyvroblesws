import {Injectable} from '@angular/core'
import { Zapatilla } from '../models/zapatilla'

@Injectable()
export class ZapatillaService{
    public zapatillas: Array<Zapatilla>

    constructor(){
        this.zapatillas = [
            new Zapatilla('nike airmax', 'nike', 'rojo', 190, true),
            new Zapatilla('reebok classic', 'reebok', 'blanco', 80, true),
            new Zapatilla('reebok spartan', 'reebok', 'negro', 180, true),
            new Zapatilla('nike runner md', 'nike', 'negro', 60, true),
            new Zapatilla('adidas yezzy', 'adidas', 'gris', 180, false)
        ]
    }

    getTexto() {
        return "Hola Mundo desde un servicio"
    }

    getZapatillas(): Array<Zapatilla>{
        return this.zapatillas
    }
}