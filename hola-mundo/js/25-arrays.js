'use strict'

//arrays
var nombres = ['Lina Maria', 'orlando arias', 'Diana Jimena'];

var lenguajes = new Array('php', 'js', 'go', 'java', 'C', 'Pascal');

// console.log(nombres);
// console.log(lenguajes);
// console.log(nombres[0]);


//array element quantity
//console.log(nombres.length);

//var element = parseInt(prompt('que elemento del arreglo quieres??', 0));
//alert(nombres[element]);

// var element = parseInt(prompt('que elemento del arreglo quieres??', 0));
// if(element >= nombres.length) {
// 	alert('introduce el numero correcto menor q ' + nombres.length);
// } else {
// 	alert('el usuario selecionado es: ' + nombres[element]);
// }
 
// document.write('<h1>lenguajes de programacion del 2020</h1>');
// document.write('<ul>');
// 	for(let i = 0; i < lenguajes.length; i++) {
// 		document.write('<li>' + lenguajes[i] + '</li>');
// 	}
// document.write('</ul>');

document.write('<h1>lenguajes de programacion del 2020</h1>');
document.write('<ul>');
	lenguajes.forEach((elemento, indice, array) => {
        console.log(array);
		document.write('<li>' + indice + '-' + elemento + '</li>');
	});
document.write('</ul>');

/*
//another alternative to iterate an array
document.write('<h1>lenguajes de programacion del 2019</h1>');
document.write('<ul>');
	for(let lenguaje in lenguajes) {
		document.write('<li>' + lenguajes[lenguaje] + '</li>');
	}
document.write('</ul>');
*/

//busquedas
/*
var busqueda = lenguajes.find(lenguaje => lenguaje == 'php');

console.log(busqueda);
*/

/*
//busquedas recuperando el indice
var busqueda = lenguajes.findIndex(lenguaje => lenguaje == 'php');

console.log(busqueda);
*/


//mas busquedas
var precios = [10, 20, 50, 80, 12];
var busqueda = precios.some(precio => precio >= 20);
//console.log(busqueda);