'use strict'

//condicional IF

// first exercise
// var edad1 = 10;
// var edad2 = 12;

// if(edad1 > edad2) {
// 	console.log('edad 1 es mayor q edad 2');
// }else {
// 	console.log('edad 1 es inferior');
// }


// var edad = 19;
// var nombre = 'crotatas mochuelo';

// if(edad >= 18) {
// 	//usuario mayor de edad
// 	console.log(nombre + ' tiene ' + edad + ' anos; es mayor de edad.');
// 	if(edad <= 33) {
// 		console.log('todavia eres millenial');
// 	}else 	if(edad >= 70) {
// 				console.log('eres anciano');
// 			}else {
// 				console.log('ya no eres millenial');
// 			}				
// }else {
// 	//usuario menor de edad
// 	console.log(nombre + ' tiene ' + edad + ' anos; es menor de edad.');
// }

//operadores logicos
var year = 2020;

//negacion
// if(year != 2016) {
// 	console.log('el ano no es 2016, realmente es: ' + year);
// }

// //and
// if(year >= 2000 &&
//    year <= 2020 &&
//    year != 2018) {
// 	console.log('estamos en la era actual.');
// } else {	
// 	console.log('estamos en la era post moderna.');
// }

// //or

// if(year == 2008 ||
//    year == 2018) {
// 	console.log('el ano acaba en 8.');
// } else {	
// 	console.log('el ano NO termina en 8.');
// }

if(year == 2008 ||
  (year >= 2018 && year == 2028)) {
	console.log('el ano acaba en 8.');
} else {
	console.log('ano no registrado.');
}