'use strict'

/*funciones anonimas
*/
// var pelicula = function(nombre) {
// 	return 'la pelicula es: ' + nombre;
// }
// console.log(pelicula('no nombre'));

//callback functions
// function sumame(numero1, numero2) {
// 	var sumar = numero1 + numero2;	
// 	return sumar;
// }
// console.log(sumame(4, 5));

// function sumame(numero1, numero2, sumaYMuestra, sumaPorDos) {
// 	var sumar = numero1 + numero2;
// 	sumaYMuestra(sumar);
// 	sumaPorDos(sumar);
// 	return sumar;
// }

// sumame(5, 7, function(dato) {
// 				console.log('la suma es: ' + dato);
// 			},
// 			function(dato) {
// 				console.log('la suma por dos es: ' + (dato * 2));
// 			}
// );

//arrow functions
function sumame(numero1, numero2, sumaYMuestra, sumaPorDos) {
	var sumar = numero1 + numero2;
	sumaYMuestra(sumar);
	sumaPorDos(sumar);
	return sumar;
}

sumame(5, 7, dato => {
				console.log('la suma es: ' + dato);
			},
			dato => {
				console.log('la suma por dos es: ' + (dato * 2));
			}
		);		