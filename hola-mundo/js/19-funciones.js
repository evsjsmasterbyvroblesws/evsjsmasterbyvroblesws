'use strict'

/*funciones
*/

function calculadora(numero1, numero2) {
	// console.log('hola soy la calculadora!');
	// console.log('si, soy yo');

	// return 'hola soy la calculadora!';

	// console.log('suma: ' + (10 + 15));
	// console.log('resta: ' + (10 - 15));
	// console.log('multiplicacion: ' + (10 * 15));
	// console.log('division: ' + (10 / 15));

	// return 'hola soy la calculadora!';

	// console.log('suma: ' + (numero1 + numero2));
	// console.log('resta: ' + (numero1 - numero2));
	// console.log('multiplicacion: ' + (numero1 * numero2));
	// console.log('division: ' + (numero1 / numero2));
	// console.log('******************************');
}
//function con parametros opcionales
// function calculadora(numero1, numero2, mostrar = false) {
// 	if(!mostrar) {
// 		console.log('suma: ' + (numero1 + numero2));
// 		console.log('resta: ' + (numero1 - numero2));
// 		console.log('multiplicacion: ' + (numero1 * numero2));
// 		console.log('division: ' + (numero1 / numero2));
// 		console.log('******************************');			
// 	} else {
// 		document.write('suma: ' + (numero1 + numero2) + '<br/>');
// 		document.write('resta: ' + (numero1 - numero2) + '<br/>');
// 		document.write('multiplicacion: ' + (numero1 * numero2) + '<br/>');
// 		document.write('division: ' + (numero1 / numero2)+ '<br/>');
// 	}
// }

//con funcion invocando funciones
function porPantalla(numero1, numero2) {
		document.write('suma: ' + (numero1 + numero2) + '<br/>');
		document.write('resta: ' + (numero1 - numero2) + '<br/>');
		document.write('multiplicacion: ' + (numero1 * numero2) + '<br/>');
		document.write('division: ' + (numero1 / numero2)+ '<br/>');
		document.write('******************************' + '<br/>');				
}

function porConsola(numero1, numero2) {
		console.log('suma: ' + (numero1 + numero2));
		console.log('resta: ' + (numero1 - numero2));
		console.log('multiplicacion: ' + (numero1 * numero2));
		console.log('division: ' + (numero1 / numero2));
		console.log('******************************');				
}
	
function calculadora(numero1, numero2, mostrar = false) {
	if(!mostrar) {
		porConsola(numero1, numero2);
	} else {
		porPantalla(numero1, numero2);
	}	
}	

//************************** */
//calling the function.
//console.log(calculadora(), calculadora(), calculadora());

//calculadora();

// var resultado = calculadora();
// console.log(resultado); //prints calculadora() return
//calculadora estatica
//calculadora();

//calculadora con parametros

// calculadora(12, 8);
// calculadora(98, 2);
// calculadora();


//calculadora con parametros opcionales
// calculadora(1, 4);
// calculadora(2, 2, true);

//calculadora con funcion invocando funciones
calculadora(1, 4);
calculadora(2, 5, true);
calculadora(4, 5, true);