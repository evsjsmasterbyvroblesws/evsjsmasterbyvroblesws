'use strict';

//DOM document object model
let caja = document.getElementById('miCaja');
function cambiaColor(color) {
    caja.style.background = color;
}

// find elements with a specific id
//returning the whole element
// console.log(caja);

//returning the content of the element
// let caja2 = document.getElementById('miCaja').innerHTML;
// console.log(caja2);

//updating the content of the element
// caja.innerHTML = 'TEXTO EN LA CAJA DESDE JS!';
// console.log(caja);


//updating styles and insering classes
// //caja.style.background = 'red';
// caja.style.padding = '20px';
// caja.style.color = 'white';
// cambiaColor('blue');
// caja.className = 'hola hola2';
// console.log(caja);


//selecting elements using query selector
// let caja1 = document.querySelector('#miCaja');
// let caja2 = document.querySelector('#miCaja').innerHTML;
// console.log(caja1);
// console.log(caja2);
// console.log(caja1, '\n' + caja2);

//conseguir elementos por su etiqueta (really it's its tag name)
var todosLosDivs = document.getElementsByTagName('div');
// console.log(todosLosDivs);

//var contenidoEnTexto = todosLosDivs[2].textContent;
// console.log(contenidoEnTexto);
// contenidoEnTexto = todosLosDivs[2].innerHTML;
// console.log(contenidoEnTexto);
// contenidoEnTexto = todosLosDivs[2];
// contenidoEnTexto.innerHTML = 'otro texto para el segundo elemento';
// console.log(contenidoEnTexto);
// contenidoEnTexto.style.background='red';
// console.log(contenidoEnTexto);

// todosLosDivs.forEach((valor, indice) => {  //fails because todosLosDivs is an html collection.
// 	let parrafo = document.createElement("p");
// 	let texto =  document.createTextNode(valor);
// 	parrafo.appendChild(texto);
// 	document.querySelector("miSesion").appendChild(parrafo);
// });

// var sesion = document.querySelector('#miSesion');
// var hr = document.createElement('hr');
// //console.log(todosLosDivs);
// let valor;
// for(valor in todosLosDivs) {
// 	//console.log(todosLosDivs[valor]);
// 	if(typeof todosLosDivs[valor].textContent == 'string') {
// 		let parrafo = document.createElement('p'); //creates the <p>
// 		let texto = document.createTextNode(todosLosDivs[valor].textContent); //retrieves and creates text
// 		parrafo.append(texto); //adds text to the <p>
// 		//document.querySelector("#miSesion").appendChild(parrafo);
// 		sesion.append(parrafo); //adds <p> to the sesion div
// 	}
// }
// sesion.append(hr);

//selecting elements by its class
// let divsRojos = document.getElementsByClassName('rojo');
// //console.log(divsRojos);
// divsRojos[0].style.background = 'red';
// let divsAmarillos = document.getElementsByClassName('amarillo');
// divsAmarillos[0].style.background = 'yellow';		
// for(let div in divsRojos) {
// 	if(divsRojos[div].className == 'rojo') {
// 		divsRojos[div].style.background = 'red';		
// 	}	
// }

//query selector
// let id = document.querySelector('#encabezado');
// console.log(id);

// let claseRojo = document.querySelector('.rojo');
// console.log(claseRojo);

// let etiqueta = document.querySelector('div');
// console.log(etiqueta);