'use strict'

/*decir si un numero es par o impar. debe incluir:
1. prompt
2. si el dato entrado no es numerico, vuelva a pedirlo. 
*/

var number = parseInt(prompt('introduce un numero', 0));

while(isNaN(number)) {
	let number = parseInt(prompt('introduce un numero', 0));	
}

if((number % 2) == 0) {
	alert(number + ' es un numero par');	
} else {
	alert(number + ' es un numero impar');	
}