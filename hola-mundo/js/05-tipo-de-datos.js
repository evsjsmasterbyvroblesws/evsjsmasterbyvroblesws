'use strict'

//operadores
var numero1 = 7;
var numero2 = 12;
var operacion = numero1 * numero2;
var operacion2 = numero1 % numero2;

//alert('el resultado de la operacion es: ' + operacion);

//tipos de datos
var nume_entero = 44;
var cadena_texto = 'hola "que" tal';
var verdadero_o_falso = true;
var numero_falso = '33.4';

console.log(cadena_texto);
console.log(Number(numero_falso) + 7);

console.log(parseInt(numero_falso) + 7);
console.log(parseFloat(numero_falso) + 7);
console.log(String(nume_entero) + ' hola mundo');
console.log(typeof nume_entero);
console.log(typeof cadena_texto);
console.log(typeof verdadero_o_falso);
console.log(typeof numero_falso);