'use strict';
/*escribir un programa q:
	1. pida 6 numeros por pantalla y los incluya en un arreglo.
	2. mostrar el arreglo entero (todos sus elementos) en el cuerpo de la pagina y en la consola.
	3. ordenarlo y mostrarlo.
	4. invertir su orden y mostrarlo.
	5. mostrar cuantos elementos tiene el arreglo.
	6. busqueda de un valor introducido por el usuario, q diga si lo encuentra y su indice.	
*/

function mostrarArray(elementos, textoCustom = ''){
	document.write('<h1>contenido del arreglo' + textoCustom + '</h1>');
	document.write('<ul>');	
		elementos.forEach((numero, index) => {
					document.write('<li>' + numero + '</li>');
					});
	document.write('</ul>');						
}

//option 1
// var numeros = new Array(6); 
// for(var i = 0; i <= 5; i++) {
// 	numeros[i] = parseInt(prompt('introduce un numero', 0));
// }
// console.log(numeros);

////option 2. much better.
//pedir los numeros
//let numeros = [];
// for(let i = 0; i <= 5; i++) {
// 	numeros.push(parseInt(prompt('introduce un numero', 0)));
// }
//mostrar el arreglo por la consola
//console.log(numeros);

//mostrar en el cuerpo de la pagina
//mostrarArray(numeros);

//ordenar y mostrar
let numeros = [4, 3, 1, 5, 0, 2];
// numeros.sort();
// console.log(numeros);
// mostrarArray(numeros, ', ordenado');

// numeros.sort((a, b) => {return a - b});
// mostrarArray(numeros, ', ascendente');

//descendent sort
// numeros.sort((a, b) => {return b - a});
// mostrarArray(numeros, ', descendente');

//invertir y mostrar
// numeros.reverse();
// mostrarArray(numeros, ', invertido');

//cuanto elementos tiene el arreglo?
//document.write('<h1>el arreglo tiene ' + numeros.length + ' elementos</h1>');

//busqueda
let busqueda = parseInt(prompt('busca un numero', 0));
let posicion = numeros.findIndex(any => any == busqueda);
if(posicion &&
   posicion != -1) {
		document.write('<h1>encontrado</h1>');
		document.write('<h1>posicion de la busqueda es: ' + posicion + '</h1>');   	
   } else {
   		document.write('<h1>no encontrado!</h1>');
   }
