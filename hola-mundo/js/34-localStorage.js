'use strict';
//checking if the browser supports it.
if(typeof(Storage) != 'undefined'){
    console.log('local storage is available');
}else{
    console.log('local storage is not available');
}

// data storage.
localStorage.setItem('titulo', 'curso de js de victor robles');

//retrieving data from the local storage.
//console.log(localStorage.getItem('titulo'));
document.querySelector('#peliculas').innerHTML = localStorage.getItem('titulo');

//store objects in the local storage.
let usuario = {
    name: 'orlando',
    email: 'oa@oa.com',
    web: 'victorroblesweb.es'
};
localStorage.setItem('usuario', JSON.stringify(usuario));

//retriving objects from the local storage.
console.log(localStorage.getItem('usuario'));
let userjs = JSON.parse(localStorage.getItem('usuario'));
console.log(userjs);
document.querySelector('#data').append(userjs.name + ' ' + userjs.email);

//clearing the local storage.
localStorage.removeItem('usuario');
localStorage.clear();