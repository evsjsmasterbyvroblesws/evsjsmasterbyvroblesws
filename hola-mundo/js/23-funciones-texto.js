'use strict'

//transformacion de textos
var numero = 444;
var texto1 = 'bienvenido al curso de javaScript de Victor Robles cursos';
var texto2 = 'ES MUY BUEN CURSO';


var dato = numero.toString(); //converts numeric to string.    
// console.log(dato);
// console.log(typeof dato);

dato = texto1.toUpperCase(); //translates text to uppercase.
//console.log(dato);

dato = texto2.toLowerCase(); //translates text to lowercase.
//console.log(dato);

//calcular longitud 
var nombre1 = '';
var nombre2 = 'Orlando';
var array = ['hola', 'hola'];
// console.log(nombre1.length); //returns 0.
// console.log(nombre2.length); //returns 7.
// console.log(array.length); //returns 2 (elements).

//concatenation
//console.log(texto1 + ' ' + texto2);
var textoTotal1 = texto1 + ' ' + texto2;
var textoTotal2 = texto1.concat(' ' + texto2);
// console.log(textoTotal1);
// console.log(textoTotal2);


//busquedas
var busqueda = texto1.indexOf('curso');
//console.log(busqueda); //returns 14 (the first occur).

var texto3 = 'bienvenido al curso de javaScript curso de Victor Robles';
busqueda = texto3.lastIndexOf('curso');
//console.log(busqueda); //returns 14 (the last occur).

var busqueda1 = texto1.search('curso');
var busqueda2 = texto1.search('oaf');
var busqueda3 = texto3.search('curso');
// console.log(busqueda1); //returns 14.
// console.log(busqueda2); //returns -1 unsuccessful.
// console.log(busqueda3); //returns 14 (always the first occur).

busqueda = texto1.match('e');
//console.log(busqueda); //returns an array (always on the first occur).
busqueda2 = texto3.match(/curso/g); //returns an array with all occurs.
//console.log(busqueda2);

busqueda = texto1.substr(14, 9);
//console.log(busqueda); //returns 'curso de '

busqueda = texto1.charAt(44);
//console.log(busqueda); //returns 'R'

busqueda = texto1.startsWith('bienvenido al');
//console.log(busqueda); //returns true. 

busqueda = texto1.endsWith('Robles cursos');
//console.log(busqueda); //returns true. 

busqueda = texto1.includes('javaScript');
//console.log(busqueda); //returns true.

busqueda = texto1.replace('e', '*'); //only the firts occur.
//console.log(busqueda);

busqueda = texto1.slice(14);
busqueda1 = texto1.slice(14, 22);
// console.log(busqueda);
// console.log(busqueda1);

busqueda = texto1.split();
busqueda1 = texto1.split(' ');
// console.log(busqueda);
// console.log(busqueda1);

busqueda = texto1.trim();
console.log(busqueda);