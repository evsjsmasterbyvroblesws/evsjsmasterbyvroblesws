'use strict';

window.addEventListener('load', () => { //similar to the document.ready() in jquery.
    //mouse events
    let boton = document.querySelector('#boton');

    function cambiarColor() {
        console.log('me has dado click');
        let bg = boton.style.backgroundColor;
        if(bg == "green") {
            boton.style.backgroundColor = 'red';
        } else {
            boton.style.backgroundColor = 'green';
        }

        boton.style.padding = '10px';
        boton.style.border = '1px solid #ccc'
        return true;       
    }

    //click event handler.
    boton.addEventListener('click', function() {
        cambiarColor();
        //boton.style.border = "10px solid black";
        //this in here refers to the boton object.
        //this doesn't worth with the => notation.
        console.log(this);
        this.style.border = "10px solid black";
    })

    //mouse over event handler.
    boton.addEventListener('mouseover', () => {
        boton.style.backgroundColor = 'yellow';
    })

    //mouse out event handler.
    boton.addEventListener('mouseout', () => {
        boton.style.backgroundColor = '#ccc';
    })

    //focus handler
    let input = document.querySelector('#campo-nombre');
    input.addEventListener('focus', () => {
        //console.log('[focus] estas dentro del input');
    })

    //blur handler
    input.addEventListener('blur', () => {
        //console.log('[blur] estas fuera del input');
    })

    //keydown handler
    input.addEventListener('keydown', (event) => {
        //console.log('[keydown] Pulsando esta tecla ', String.fromCharCode(event.keyCode));
    })

    //keypress handler
    input.addEventListener('keypress', (event) => {
        //console.log('[keypress] tecla presionada ', String.fromCharCode(event.keyCode));
    })

    //keyup handler
    input.addEventListener('keyup', () => {
        //console.log('[keyup] tecla liberada ', String.fromCharCode(event.keyCode));
    })
}) //end of load()
