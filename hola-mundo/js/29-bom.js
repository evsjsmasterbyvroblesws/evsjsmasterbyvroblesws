'use strict';

//BOM browser object model
function getBom() {
	console.log(window.innerHeight);//returns the height of browser screen in pixels
	console.log(window.innerWidth); //returns the width of browser screen in pixels
	console.log(screen.height);//returns the height of screen in pixels
	console.log(screen.width); //returns the width of  screen in pixels
	console.log(window.location); //returns the location object
	console.log(window.location.href); //returns the current url (it's part of the location object)
}

function redirect(url) {
	window.location.href = url; //goes to the new url
}

function abrirVentana(url) {
	//window.open(url);	//opens a new tab with info from the specified url
	window.open(url, "", "width=400, height=300"); //opens a new window with info from the specified url, weight and height 	
}
//getBom();
//redirect('https://google.com');
//redirect('https://victorroblesweb.es');

//abrirVentana('https://google.com');
abrirVentana('https://victorroblesweb.es');
//warning: the nex one opens 21 windows.
//abrirVentana('file:///C:/eVSJSMasterByVRoblesWS/hola-mundo/index.html');