'use strict';
//lesson 79
//fetch y peticiones a servicios/apis rest.
//(peticiones ajax, asincronas a un servidor).

//let usuarios = []; //code commented for lesson 80.
//let div_usuarios = document.querySelector('#usuarios');
//with jsonplacehloder:
// fetch('https://jsonplaceholder.typicode.com/users')
// .then(data => data.json())
// .then(data => {
//     usuarios = data;
//     console.log(usuarios);
// })

//with reqres.in: //code commented for lesson 80.
// fetch('https://reqres.in/api/users')
// .then(data => data.json())
// .then(users => {
//      usuarios = users.data;
//      console.log(usuarios);

//     usuarios.map((user, i) => { //renders in html page.
//         let nombre = document.createElement('h3');
//         nombre.innerHTML = i + '. ' + user.first_name + ' ' + user.last_name;
//         div_usuarios.appendChild(nombre);
//         document.querySelector('.loading').style.display = 'none';
//     });
//});

//with my rest service: doesn't work. it works for get, not fetch.
// get('http://localhost:8080/users') ????
// .then(data => data.json())
// .then(users => {
//     usuarios = users.data;
//     console.log(usuarios);

//     usuarios.map((user, i) => { //renders in html page.
//         let nombre = document.createElement('h3');
//         nombre.innerHTML = i + '. ' + user.first_name + ' ' + user.last_name;
//         div_usuarios.appendChild(nombre);
//         document.querySelector('.loading').style.display = 'none';
//     });
// })
////////////////////////////////////////////////////////////////////////////////////////////////////////

//lesson 80
// getUsuarios()
//             .then(data => data.json())
//             .then(users => {
//                 listadoUsuarios(users.data);

//                 return getJanet();
//             })
//             .then(data => data.json())
//             .then(user => {
//                 mostrarJanet(user.data);
//             });

// function getUsuarios(){
//     return fetch('https://reqres.in/api/users');
// }

// function getJanet(){
//     return fetch('https://reqres.in/api/users/2');
// }

// function listadoUsuarios(usuarios){
//     usuarios.map((user, i) => { //renders in html page.
//         let nombre = document.createElement('h3');
//         nombre.innerHTML = i + '. ' + user.first_name + ' ' + user.last_name;
//         div_usuarios.appendChild(nombre);
//         document.querySelector('.loading').style.display = 'none';
//     });
// }

// let div_janet = document.querySelector('#janet');
// function mostrarJanet(user){
//         let nombre = document.createElement('h4');
//         let avatar = document.createElement('img');

//         nombre.innerHTML = user.first_name + ' ' + user.last_name;
//         avatar.src = user.avatar;
//         avatar.width = '100';

//         div_janet.appendChild(nombre);
//         div_janet.appendChild(avatar);
//         document.querySelector('#janet .loading').style.display = 'none';
// }
////////////////////////////////////////////////////////////////////////////////////////////////////////

//lesson 81
let div_usuarios = document.querySelector('#usuarios');
let div_janet = document.querySelector('#janet');
let div_profesor = document.querySelector('#profesor');
getUsuarios()
            .then(data => data.json())
            .then(users => {
                listadoUsuarios(users.data);

                return getInfo();                
            })
            .then(data => {
                div_profesor.innerHTML = data;
                
                return getJanet();
            })
            .then(data => data.json())
            .then(user => {
                mostrarJanet(user.data);                
            })
            .catch(error => {
                console.log(error);
            });
            

function getUsuarios(){
    return fetch('https://reqres.in/api/users');
}

function getJanet(){
    return fetch('https://reqres.in/api/users/2');
}

function getInfo() {
    var profesor = {
        nombre: 'Orlando',
        apellidos: 'Arias',
        url: 'https://abcdefg.com'
    };
    
    return new Promise((resolve, reject) => {
        var profesor_string = '';
        setTimeout(function(){
            profesor_string = JSON.stringify(profesor);
            if(typeof(profesor_string) != 'string' ||
                  profesor_string == '') return reject('error 1');
                  return resolve(profesor_string);
        }, 3000);
                
        
    });
    
}

function listadoUsuarios(usuarios){
    usuarios.map((user, i) => { //renders in html page.
        let nombre = document.createElement('h3');
        nombre.innerHTML = i + '. ' + user.first_name + ' ' + user.last_name;
        div_usuarios.appendChild(nombre);
        document.querySelector('.loading').style.display = 'none';
    });
}

function mostrarJanet(user){
        let nombre = document.createElement('h4');
        let avatar = document.createElement('img');

        nombre.innerHTML = user.first_name + ' ' + user.last_name;
        avatar.src = user.avatar;
        avatar.width = '100';

        div_janet.appendChild(nombre);
        div_janet.appendChild(avatar);
        document.querySelector('#janet .loading').style.display = 'none';
}