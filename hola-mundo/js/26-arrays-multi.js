'use strict';

//multidimensional arrays
var categorias = ['accion', 'terror', 'comedia'];
var peliculas = ['la verdad duele', 'la vida es bella', 'gran torino'];

var cine = [categorias, peliculas];

//console.log(cine);

//accessing a specific element
// console.log(cine[0][1]); //returns 'terror'
// console.log(cine[1][2]); //returns 'gran torino'

//adding one element at the end of the array
//peliculas.push('batman');
//console.log(peliculas);

// var elemento = '';
// do {
// 	elemento = prompt('introduzca tu pelicula:');
// 	peliculas.push(elemento);
// } while(elemento != 'ACABAR');

// //removing the last element
// peliculas.pop();
// console.log(peliculas);

// console.log(peliculas);
// peliculas[0] = undefined; //replaces the 0 element with undefined.
// console.log(peliculas);

//finding the index of a specific element and removing it 
// var indice = peliculas.indexOf('gran torino');
// console.log(indice);
// console.log(peliculas);

// if(indice > -1) {
//  peliculas.splice(indice, 1); //removes the elements with [indice], just one element.
// }
// console.log(peliculas);

// if(indice > -1) {
//     peliculas.splice(indice, 1); //removes elements starting with [indice].
//    }
//    console.log(peliculas);

//converting an array to string
// var array_to_string = peliculas.join();
// console.log(peliculas);
// console.log(array_to_string);

//converting from string to array
var cadena = 'texto1, texto2, texto3';
var cadena_array = cadena.split(', ');
//console.log(cadena_array);


//sorting and reversing an array
// console.log(peliculas);
// peliculas.sort();
// console.log(peliculas);
// peliculas.reverse();
// console.log(peliculas);

//another alternative to iterate an array
var lenguajes = new Array('php', 'js', 'go', 'java', 'C', 'Pascal');
// document.write('<h1>lenguajes de programacion del 2020</h1>');
// document.write('<ul>');
// 	for(let lenguaje in lenguajes) {
// 		document.write('<li>' + lenguajes[lenguaje] + '</li>');
// 	}
// document.write('</ul>');

//busquedas
// var busqueda = lenguajes.find(function(lenguaje) {
//     return lenguaje == 'php';
// });
// console.log(busqueda);
// //improved example
// var busqueda = lenguajes.find(lenguaje => lenguaje == 'js');
// console.log(busqueda);

//busquedas recuperando el indice
// var busqueda = lenguajes.findIndex(lenguaje => lenguaje == 'php');
// console.log(busqueda);

//mas busquedas
var precios = [10, 20, 50, 80, 12];
var busqueda = precios.some(precio => precio >= 20);
console.log(busqueda);