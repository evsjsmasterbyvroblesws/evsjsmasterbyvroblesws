'use strict'

/*parametros REST y SPREAD
*/
// function listadoFrutas(fruta1, fruta2) {
// 	console.log('fruta 1', fruta1);
// 	console.log('fruta 2', fruta2);
// }

// listadoFrutas('naranja', 'manzana');

// function listadoFrutas(fruta1, fruta2, ...resto_de_frutas) {
// 	console.log('fruta 1', fruta1);
// 	console.log('fruta 2', fruta2);
// 	console.log(resto_de_frutas);
// }
// listadoFrutas('naranja', 'manzana');
// //parametros tipo REST
// listadoFrutas('naranja', 'manzana', 'sandia', 'pera', 'melon', 'coco');

// //parametros tipo SPREAD
function listadoFrutas(fruta1, fruta2, ...resto_de_frutas) {
	console.log('fruta 1', fruta1);
	console.log('fruta 2', fruta2);
	console.log(resto_de_frutas);
}
var frutas = ['naranja', 'manzana'];
listadoFrutas(...frutas, 'sandia', 'pera', 'melon', 'coco');