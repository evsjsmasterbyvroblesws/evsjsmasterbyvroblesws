'use strict';
let formulario = document.querySelector('#moviesForm')

formulario.addEventListener('submit', function() {
    //console.log('I am here!'); doesn't work
    let titulo = document.querySelector('#addMovie').value;
    //localStorage.clear();
    if(titulo.length >= 1){
        localStorage.setItem(titulo, titulo);
    }    
});

let ul = document.querySelector('#movie-list');
for(let i in localStorage){
    console.log(localStorage[i]);
    if(typeof(localStorage[i]) == 'string'){
        let li = document.createElement('li');
        li.append(localStorage[i]);
        ul.append(li);
    }    
}

let formularioB = document.querySelector('#deleteMovieForm');
formularioB.addEventListener('submit', function() {
    let titulo = document.querySelector('#deleteMovie').value;
    if(titulo.length >= 1){
        localStorage.removeItem(titulo);
    }    
});