'use strict'
//pruebas con let y var

//prueba con var
var numero = 40;
console.log(numero); //valor = 40

if(true) {
	var numero = 50;
	console.log(numero); //valor = 50
}

console.log(numero); //valor = 50

//prueba con let
var texto = "curso de js";
console.log(texto); //valor = curso de js

if(true) {
	let texto = "curso de Laravel 5";
	console.log(texto); //valor = curso de Laravel 5
}

console.log(texto); //valor = curso de js