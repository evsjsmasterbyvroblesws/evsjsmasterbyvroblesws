'use strict';
window.addEventListener('load', () => {
    //timers
    // let tiempo = setInterval( () => {
    //     console.log('Set interval ejecutado')
    //     //document.querySelector('h1').style.fontSize='50px';
    //     let encabezado = document.querySelector('h1');
    //     if(encabezado.style.fontSize == '50px'){
    //         encabezado.style.fontSize = '30px';
    //     }else{
    //         encabezado.style.fontSize = '50px';
    //     }        
    // }, 1000);

    // let tiempo = setTimeout( () => {
    //     console.log('Set interval ejecutado')
    //     //document.querySelector('h1').style.fontSize='50px';
    //     let encabezado = document.querySelector('h1');
    //     if(encabezado.style.fontSize == '50px'){
    //         encabezado.style.fontSize = '30px';
    //     }else{
    //         encabezado.style.fontSize = '50px';
    //     }        
    // }, 3000)

    let tiempo = intervalo();
    let stop = document.querySelector('#stop');
    stop.addEventListener('click', () => {
        clearInterval(tiempo);
    });

    let start = document.querySelector('#start');
    start.addEventListener('click', () => {
        tiempo = intervalo();
    })

    function intervalo() {
        let tiempo = setInterval( () => {
            console.log('Set interval ejecutado')            
            let encabezado = document.querySelector('h1');
            if(encabezado.style.fontSize == '50px'){
                encabezado.style.fontSize = '30px';
            }else{
                encabezado.style.fontSize = '50px';
            }        
        }, 1000);
        return tiempo;
    }
});