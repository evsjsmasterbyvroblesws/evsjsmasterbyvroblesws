'use strict'

//programa q pida dos numeros y q nos diga cual es mayor, el menor y si son iguales

// var numero1 = parseInt(prompt('introduce el primer numero',  0));
// var numero2 = parseInt(prompt('introduce el segundo numero', 0));

// console.log(numero1, numero2);

// if(numero1 == numero2) {
// 	alert('los numeros son iguales');
// } else if(numero1 > numero2) {
// 	alert('el numero mayor es: ' + numero1);
// 	alert('el numero menor es: ' + numero2);
// } else {
// 	alert('el numero mayor es: ' + numero2);
// 	alert('el numero menor es: ' + numero1);	
// }


/*programa q pida dos numeros y q nos diga cual es mayor, el menor y si son iguales
  plus: si los datos entrados no son numericos o son menores o iguales a cero, vuelva a pedir los numeros.
*/

var numero1 = parseInt(prompt('introduce el primer numero',  0));
var numero2 = parseInt(prompt('introduce el segundo numero', 0));

while(numero1 <= 0  || 
	  numero2 <= 0  ||
	  isNaN(numero1)||
	  isNaN(numero2)) {
	  	numero1 = parseInt(prompt('introduce el primer numero',  0));
		numero2 = parseInt(prompt('introduce el segundo numero', 0));

}
if(numero1 == numero2) {
	alert('los numeros son iguales');
} else if(numero1 > numero2) {
	alert('el numero mayor es: ' + numero1);
	alert('el numero menor es: ' + numero2);
} else {
	alert('el numero mayor es: ' + numero2);
	alert('el numero menor es: ' + numero1);	
}