'use strict'

//bucle while
// var year = 2018;
// while(year <= 2051) {
// 	console.log('estamos en el ano: ' + year);
// 	year++;
// }

//do while
var years = 30;
do {
	console.log('estamos en: ' + years);	
	if (years == 28){
		break;
	}
	years--;
} while (years > 25);