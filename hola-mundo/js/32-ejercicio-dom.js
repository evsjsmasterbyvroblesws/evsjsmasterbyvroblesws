'use strict';
window.addEventListener('load', () => {
    let formulario = document.querySelector('#formulario');
    let dashed_box = document.querySelector('.dashed');
    dashed_box.style.display = 'none';

    formulario.addEventListener('submit', () => {
        console.log('evento submit capturado');
        
        let nombre = document.querySelector('#nombre').value;
        let apellidos = document.querySelector('#apellidos').value;
        let edad = parseInt(document.querySelector('#edad').value);

        //validations
        if(nombre.trim() == null ||
           nombre.trim().length == 0){
            //alert("el nombre no es valido");
            document.querySelector("#error_nombre").innerHTML = "el nombre no es valido";
            return false;
        }else{
                document.querySelector("#error_nombre").style.display = "none";
        }

        if(apellidos.trim() == null ||
           apellidos.trim().length == 0){
            alert("los apellidos no son validos");
            return false;
        }

        if(edad == null ||
           edad.length <= 0 ||
           isNaN(edad)){
            alert("la edad no es valida");
            return false;
        }



        dashed_box.style.display = 'block';

        //console.log(nombre, apellidos, edad);

        //for alternative 1:
        // let datos_usuario = [nombre, apellidos, edad];

        // for(let indice in datos_usuario) {
        //     let parrafo = document.createElement("p");
        //     parrafo.append(datos_usuario[indice]);
        //     dashed_box.append(parrafo);
        // } 
        
        //for alternative 2:
        let p_nombre = document.querySelector("#p_nombre span");
        let p_apellidos = document.querySelector("#p_apellidos span");
        let p_edad = document.querySelector("#p_edad span");

        p_nombre.innerHTML = nombre;
        p_apellidos.innerHTML = apellidos;
        p_edad.innerHTML = edad;
    })
});