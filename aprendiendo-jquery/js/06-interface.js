'use strict'
$(document).ready(function(){
    //console.log('i am here')
    //mover elemente por la pagina.
    $('.elemento').draggable()

    //redimensionar.
    $('.elemento').resizable()

    //seleccionar y ordenar elementos.
    //$('.lista-seleccionable').selectable()
    $('.lista-seleccionable').sortable({
        update: function(event, ui) {
            console.log('ha cambiado la lista')
        } //end of update
    })//end of sortable

    //drag & drop
    $('#elemento-movido').draggable()
    $('#area').droppable({
        drop: function(){
            console.log('has soltado algo dentro del area')
        }
    })

    //efectos
    $('#mostrar').click(function(){
        //$('.caja-efectos').toggle('fade')
        //$('.caja-efectos').fadeToggle()
        //$('.caja-efectos').effect('explode')
        //$('.caja-efectos').toggle('explode')
        //$('.caja-efectos').toggle('blind')
        //$('.caja-efectos').toggle('slide')
        //$('.caja-efectos').toggle('drop')
        //$('.caja-efectos').toggle('fold')
        //$('.caja-efectos').toggle('puff')
        //$('.caja-efectos').toggle('scale')
        //$('.caja-efectos').toggle('shake', 'slow')
        $('.caja-efectos').toggle('shake', 4000)
        //$('.caja-efectos').toggle('shake', {json object}, 'slow')
    })

    //tooltip
    $(document).tooltip()

    //dialog
    $('#lanzar-popup').click(function(){
        $('#popup').dialog() 
    })//end of click
    
    //calendario (datepicker)
    $('#calendario').datepicker()

    //tabs
    $("#pestanas").tabs()
})//end ready().
