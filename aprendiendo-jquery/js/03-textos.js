'use strict';
$(document).ready(function(){
    //alert('i am here');
    //console.log($('a').length);

    reloadLinks(); 

    $('#add_button')
    .removeAttr('disabled')
    .click(function(){
        //console.log($('#add_link').val());
        //html overrites the list.
        //$('#menu').html('<li><a href="'+ $("#add_link").val() +'"></a></li>');
        //append adds at the end of the list.
        //$('#menu').append('<li><a href="'+ $("#add_link").val() +'"></a></li>');
        //prepend adds on top of the list.
        $('#menu').prepend('<li><a href="'+ $("#add_link").val() +'"></a></li>');
        //before adds out of the list before it.
        //$('#menu').before('<li><a href="'+ $("#add_link").val() +'"></a></li>');
        //before adds out of the list after it.
        //$('#menu').after('<li><a href="'+ $("#add_link").val() +'"></a></li>');
        
        $('#add_link').val('');
        reloadLinks(); 
    });       
});//end of ready

function reloadLinks(){
    $('a').each(function(index){
        let that = $(this);
        // console.log(that);
        // console.log($(that).attr('href'));
        let enlace = $(that).attr('href');
        
        that.attr('target', '_blank'); //opens in a new tab
        that.text(enlace);
    });//end of each
}