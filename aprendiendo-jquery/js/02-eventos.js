'use strict';
$(document).ready(function(){
    //alert('evento cargado');

    let caja = $('#caja');

    //lesson 96
    // caja.mouseover(function(){
    //     $(this).css('background', 'red');
    // });

    // caja.mouseout(function(){
    //     $(this).css('background', 'green');
    // });

    //lesson 97
    function cambiaRojo(){
            $(this).css('background', 'red');
    }

    function cambiaVerde(){
            $(this).css('background', 'green');
    } 

    caja.hover(cambiaRojo, cambiaVerde);

    //lesson 98
    caja.click(function(){
        $(this).css('background', 'blue')
               .css('color', 'white');
    });
    caja.dblclick(function(){
        $(this).css('background', 'pink')
               .css('color', 'yellow');
    });

    //lesson 99
    //focus and blur
    let nombre = $('#nombre');
    let datos = $('#datos');
    nombre.focus(function(){
        $(this).css('border', '2px solid green');
    });
    nombre.blur(function(){
        $(this).css('border', '1px solid #ccc');        
        datos.text($(this).val()).show();
    });

    //lesson 100
    //mousedown and mouseup
    datos.mousedown(function(){
        $(this).css('border-color', 'gray');
    });
    datos.mouseup(function(){
        $(this).css('border-color', 'black');
    });

    //mousemove
    $(document).mousemove(function(event){
        // console.log('en X: ' + event.pageX);
        // console.log('en Y: ' + event.pageY);

        $('body').css('cursor', 'none');
        $('#sigueme').css('left', event.pageX)
               .css('top', event.pageY);
    });            
});//end of ready()