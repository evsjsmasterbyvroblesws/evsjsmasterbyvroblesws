'use strict';
$(document).ready(function(){
    //alert('i am here!');
    //load()
    //$('#datos').load("https://reqres.in/");

    //.get & .post
    $.get("https://reqres.in/api/users", 
          {page: 2},
          function(response){
            //console.log(response);
            response.data.forEach((element, index) => {
                $('#datos').append("<p>" + element.first_name + ' ' + element.last_name + "</p>");
            }); //end of forEach.
          } //end of callback function.
    ); //end of get.

    //this portion of code was commented for lesson 107.
    // let usuario = {
    //     name: 'nicholas',
    //     web: 'nicholas.com'
    // };
    // $.post("https://reqres.in/api/users", 
    //        usuario,
    //        function(response){
    //            console.log(response);
    //        } //end of callback function.
    // ); //end of post.

    //lesson 107.
    $('#formulario').submit(function(e){
        e.preventDefault();

        let usuario = {
            name: $('input[name="name"]').val(),
            web: $('input[name="web"]').val()
        };
    //     console.log(usuario);

    //     $.post($(this).attr('action'), 
    //            usuario,
    //            function(response){
    //                console.log(response);
    //            } //end of callback function.
    //     ).done(function(){
    //         alert('Usuario fue agregado');
    //     }); //end of done & post.
        
    //     return false;
    // }); //end of callback function [submit()].

    //lesson 108.
    //.ajax()
        $.ajax({
            type: 'POST',
            // dataType: 'json', //not used
            // contentType: 'application/json', //not used
            url: $(this).attr('action'),
            data: usuario,
            beforeSend: function(){
                console.log('enviando usuario...');
            }, //end of beforeSend()
            success: function(response){
                console.log(response);
            }, //end of success()
            error: function(){
                console.log('ha ocurrido un error')
            }, //end of error()
            //timeout: 2000
            timeout: 100 //to force error.
        }); //end of .ajax().
    return false;
    }); //end of callback function [submit()].
}); //end of ready.