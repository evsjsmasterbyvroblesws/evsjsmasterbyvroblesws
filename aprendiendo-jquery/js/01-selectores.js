'use strict';
$(document).ready(()=>{
    //console.log('estamos listos');
    // let rojo = $('#rojo');
    let rojo = $('#rojo').css('background', 'red')
                         .css('color', 'white');
    //console.log(rojo);

    let amarillo = $('#amarillo').css('background', 'yellow')
                         .css('color', 'green');
    $('#verde').css('background', 'green')
               .css('color', 'white');

    let mi_clase = $('.zebra');
    // console.log(mi_clase);
    // console.log(mi_clase[0]);
    // console.log(mi_clase.eq(1));
    mi_clase.css('border', '5px dashed black');

    $('.sin_borde').click(function() {
        $(this).css('border', '5px dashed black');
    });

    //<tag> selectors
    let parrafos = $('p').css('cursor', 'pointer');
    parrafos.click(function(){
        let myThis = $(this);
        if(myThis.hasClass('zebra')){
            myThis.css('font-size', '30px');
        }
    });

    //attribute selectors
    $('[title="Google"]').css('background', '#ccc');
    $('[title="Facebook"]').css('background', 'blue');

    //others
    //$('p, a').addClass('margen-superior')

    //let busqueda = $('#caja').find('.resaltado');
    // let busqueda = $('#caja .resaltado'); //same result
    // console.log(busqueda);

    //another case:
    //let busqueda = $('#caja .resaltado'); //here the search in inside of #caja.
    /*suppose that I want to search something inside of 
    first set of <li>s
    */
    //let busqueda = $('#caja .resaltado').find('[title="Google"]');//this doesn't work.
    //solution:
    //let busqueda = $('#caja .resaltado').parent().find('[title="Google"]');//this doesn't work. 
                                                                             //this expands the search one container up.
    //let busqueda = $('#caja .resaltado').eq(0).parent().find('[title="Google"]');//no changes. Selects ul.
    //let busqueda = $('#caja .resaltado').eq(0).parent().parent().find('[title="Google"]');//no changes. Selects div#caja.
    let busqueda = $('#caja .resaltado').eq(0).parent().parent().parent().find('[title="Google"]');//Selects <a>.
    console.log(busqueda);   
}); //end of ready()
